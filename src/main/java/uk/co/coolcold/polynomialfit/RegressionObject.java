package polynomialfit;

import java.util.Arrays;

//import com.ibm.gsk.ikeyman.error.NotImplementedException;

/**
 * @author SikSo
 *
 */
public class RegressionObject {
	public static final int MAX_DEGREE = 5;
	private int degree;
	private boolean finished;

	private double[] SumX = new double[2 * MAX_DEGREE + 1];
	private double[] SumYX = new double[MAX_DEGREE + 1];

	private double[][] Matrix = new double[MAX_DEGREE + 1][MAX_DEGREE + 2];
	private double[] Coeff = new double[MAX_DEGREE + 1];

	/**
	 * Get degree of freedom set
	 * 
	 * @return Degree of freedom
	 */
	public int getDegree() {
		return degree;
	}

	/**
	 * Set degree of freedom required for polynomial
	 * 
	 * @param degree
	 */
	public void setDegree(int degree) {
		if (degree < 0 || degree > MAX_DEGREE) {
			throw new IllegalArgumentException("Degree range is  0 to " + MAX_DEGREE);
		}
		this.Clear();
		this.degree = degree;
	}

	/**
	 * Get coefficient of certain index
	 * 
	 * @param degree
	 *            0 = constant, 1 = slope etc...
	 * @return coefficient value
	 */
	public double getCoeff(int degree) {
		if (degree < 0 || degree >= MAX_DEGREE) {
			throw new IllegalArgumentException("Degree range is  0 to " + (MAX_DEGREE - 1));
		}

		// make sure it is solved
		Solve();
		return Coeff[degree];

		// int index = Math.abs(degree);
		// int tmp_order = MAX_DEGREE;
		// if(tmp_order>=getCount()) tmp_order = getCount()-1;
		// if (tmp_order<index) return 0;
		//
		// return Coeff[index];
	}

	/**
	 * Constructor, default degree to 1
	 */
	RegressionObject() {
		this.Clear();
		setDegree(1);
	}

	/**
	 * Constructor with setting of degree of freedom
	 * 
	 * @param degree
	 *            degree of freedom
	 */
	RegressionObject(int degree) {
		this.Clear();
		setDegree(degree);
	}

	/////////////////////// private methods
	/**
	 * Solve equation
	 */
	private void Solve() {
		// do nothing if already solved
		if (this.finished)
			return;

		// whatever reason that this got returned, this is considered solved
		this.finished = true;

		int order = degree;
		if (this.getCount() <= order)
			order = this.getCount() - 1;

		// if no data, don't need to solve
		if (order < 0)
			return;

		this.BuildMatrix(order);
		try {
			this.GaussSolve(order);
		} catch (Exception e) {
			System.out.println("GaussSolve cause error");
			while (order > 1) {
				this.Coeff[0] = 0;
				order = order - 1;
				this.FinalizeMatrix(order);
			}
		}

	}

	/**
	 * gaussian algorithm implementation to solve matrix, only execute by Solve
	 * 
	 * @param order
	 */
	private void GaussSolve(int order) {
		// gauss algorithm implementation,
		// following R.Sedgewick's "Algorithms in C", Addison-Wesley, with minor
		// modifications
		int o1 = order + 1;
		int max;
		double T;

		// triangulize the matrix
		for (int i = 0; i <= order; i++) {
			max = i;
			T = Math.abs(this.Matrix[max][i]);

			// find the line with the largest abs value in this row
			for (int j = i + 1; j <= order; j++) {
				if (T < Math.abs(this.Matrix[j][i])) {
					max = j;
					T = Math.abs(this.Matrix[max][i]);
				}
			}

			// exchange the two lines
			if (i < max) {
				for (int k = i; k <= o1; k++) {
					T = this.Matrix[i][k];
					this.Matrix[i][k] = this.Matrix[max][k];
					this.Matrix[max][k] = T;
				}
			}

			// scale all following lines to have a leading zero
			for (int j = i + 1; j <= order; j++) {
				//if (this.Matrix[i][i] != 0) {
					T = this.Matrix[j][i] / this.Matrix[i][i];
				//} else {
					//T = 0;
				//}

				this.Matrix[j][i] = 0;
				for (int k = i + 1; k <= o1; k++) {
					this.Matrix[j][k] -= (this.Matrix[i][k] * T);
				}
			}
		}

		// then substitute the coefficients
		for (int j = order; j >= 0; j--) {
			T = this.Matrix[j][o1];
			for (int k = j + 1; k <= order; k++) {
				T = T - this.Matrix[j][k] * this.Coeff[k];
			}
			//if (this.Matrix[j][j] != 0) {
				this.Coeff[j] = T / this.Matrix[j][j];
			//} else {
				//this.Coeff[j] = 0;
			//}
		}
		this.finished = true;
	}

	/**
	 * Build the matrix, only execute by Solve
	 * 
	 * @param order
	 *            order required for calculation
	 */
	private void BuildMatrix(int order) {
		int o1 = order + 1;
		for (int i = 0; i <= order; i++) {
			for (int k = 0; k <= order; k++) {
				this.Matrix[i][k] = this.SumX[i + k];
			}
			this.Matrix[i][o1] = this.SumYX[i];
		}
	}

	/**
	 * Finalise the matrix, only execute by Solve
	 * 
	 * @param order
	 *            order required for calculation
	 */
	private void FinalizeMatrix(int order) {
		int o1 = order + 1;
		for (int i = 0; i <= order; i++) {
			Matrix[i][o1] = SumYX[i];
		}
	}

	/////////////////////// public methods
	/**
	 * Reset the object to be used again. Does not reset degree
	 */
	public void Clear() {
		this.finished = false;

		for (int i = 0; i < this.SumX.length; i++) {
			this.SumX[i] = 0;
		}

		for (int i = 0; i < this.SumYX.length; i++) {
			this.SumYX[i] = 0;
		}

		for (double[] row : this.Matrix) {
			Arrays.fill(row, 0);
		}

		// for(int i=0;i<this.SumX.length;i++) {
		// this.SumX[i] = 0;
		// }

		for (int i = 0; i < this.Coeff.length; i++) {
			this.Coeff[i] = 0;
		}
		// this.SumX = new double[2 * MAX_DEGREE + 1];
		// this.SumYX = new double[MAX_DEGREE + 1];
		//
		// this.Matrix = new double[MAX_DEGREE + 1][MAX_DEGREE + 2];
		// this.Coeff = new double[MAX_DEGREE + 1];
	}

	/**
	 * Get the number of data added to class
	 * 
	 * @return number of data
	 */
	public int getCount() {
		return (int) this.SumX[0];
	}

	/**
	 * Add x, y pair to calculation
	 * 
	 * @param x
	 *            x value of coordinate pair
	 * @param y
	 *            y value of coordinate pair
	 */
	public void Add(int x, int y) {
		double TX = 1;
		this.finished = false;
		int max = 2 * this.degree;

		this.SumX[0]++;
		this.SumYX[0] = this.SumYX[0] + y;

		for (int i = 1; i <= this.degree; i++) {
			TX *= x;
			this.SumX[i] += TX;
			this.SumYX[i] += y * TX;
		}

		for (int i = degree + 1; i <= max; i++) {
			TX *= x;
			this.SumX[i] += TX;
		}
	}

	/**
	 * Estimate Y value based on X given least square fit on data
	 * 
	 * @param x
	 * @return y value on curve
	 */
	public double EstimatedY(int x) {
		double result = 0;

		for (int i = this.getDegree(); i >= 0; i--) {
			result = result * x + getCoeff(i);
		}

		return result;
	}

	/**
	 * Estimate Y value based on X given least square fit on data
	 * 
	 * @param x
	 * @return y value on curve
	 */
	public double dY(int x) {
		double result = 0;

		for (int i = this.getDegree(); i >= 1; i--) {
			result = result * x + getCoeff(i) * (i);
		}

		return result;
	}
}
