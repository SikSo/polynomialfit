/**
 * @author SikSo
 *
 */
package polynomialfit;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.RoundingMode;
import java.text.DecimalFormat;

//import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.function.Executable;

class RegressionObjectTest {
	RegressionObject reg;
	int[] x = { 1, 2, 3, 4, 5 };
	int[] y = { 5, 8, 3, 5, 7 };
	int degree = 3;
	DecimalFormat df = new DecimalFormat("#.##");

	@BeforeEach
	public void initialize() {
		df.setRoundingMode(RoundingMode.HALF_UP);
		reg = new RegressionObject(this.degree);
		for (int i = 0; i < x.length; i++) {
			reg.Add(this.x[i], this.y[i]);
		}
	}

	/**
	 * Test method for {@link polynomialfit.RegressionObject#getDegree()}.
	 */
	@Test
	void testGetDegree() {
		int t = 1;
		reg.setDegree(t);
		assertEquals(t, reg.getDegree());
	}

	/**
	 * Test method for {@link polynomialfit.RegressionObject#setDegree(int)}.
	 */
	@Test
	void testSetDegree_basicMax() {
		int t = RegressionObject.MAX_DEGREE;
		reg.setDegree(t);
		assertEquals(t, reg.getDegree());
	}

	@Test
	void testSetDegree_basicMin() {
		int t = 0;
		reg.setDegree(t);
		assertEquals(t, reg.getDegree());
	}

	@Test
	void testSetDegree_failMax() {
		final int t1 = RegressionObject.MAX_DEGREE + 1;
		assertThrows(IllegalArgumentException.class, () -> {
			reg.setDegree(t1);
		});
	}

	@Test
	void testSetDegree_failMin() {
		final int t2 = -1;
		assertThrows(IllegalArgumentException.class, () -> {
			reg.setDegree(t2);
		});
	}

	/**
	 * Test method for {@link polynomialfit.RegressionObject#getCoeff(int)}.
	 */
	@Test
	void testGetCoeff() {
		assertEquals(df.format(0), df.format(reg.getCoeff(4)));
		assertEquals(df.format(0.6667), df.format(reg.getCoeff(3)));
		assertEquals(df.format(-5.6429), df.format(reg.getCoeff(2)));
		assertEquals(df.format(13.69), df.format(reg.getCoeff(1)));
		assertEquals(df.format(-3.4), df.format(reg.getCoeff(0)));
	}

	/**
	 * Test method for {@link polynomialfit.RegressionObject#RegressionObject()}.
	 */
	@Test
	void testRegressionObject() {
		reg = new RegressionObject();
		assertEquals(1, reg.getDegree());
	}

	/**
	 * Test method for {@link polynomialfit.RegressionObject#RegressionObject(int)}.
	 */
	@Test
	void testRegressionObjectInt() {
		int t = 2;
		reg = new RegressionObject(t);
		assertEquals(t, reg.getDegree());
	}

	/**
	 * Test method for {@link polynomialfit.RegressionObject#Clear()}.
	 */
	@Test
	void testClear() {
		int t = reg.getCount();
		if (t == 0) {
			fail("class not initialised");
		}
		reg.Clear();
		assertEquals(0, reg.getCount());
	}

	/**
	 * Test method for {@link polynomialfit.RegressionObject#getCount()}.
	 */
	@Test
	void testGetCount() {
		assertEquals(this.x.length, reg.getCount());
	}

	/**
	 * Test method for {@link polynomialfit.RegressionObject#Add(int, int)}.
	 */
	@Test
	void testAdd() {
		int t = reg.getCount();
		reg.Add(12, 24);
		assertEquals(t + 1, reg.getCount());
	}

	/**
	 * Test method for {@link polynomialfit.RegressionObject#EstimatedY(int)}.
	 */
	@Test
	void testEstimatedY() {
		assertEquals(df.format(351.74),df.format(reg.EstimatedY(11)));
	}

	/**
	 * Test method for {@link polynomialfit.RegressionObject#dY(int)}.
	 */
	@Test
	void testdY() {
		assertEquals(df.format(131.547619),df.format(reg.dY(11)));
	}
}
